package com.appnomic.appsone.connectors.SAP;

import com.appnomic.appsone.connectors.utill.ConfigInitializer;
import com.sap.conn.jco.*;
import org.apache.log4j.Logger;

import java.util.StringTokenizer;

public class Connection {
    private static final Logger log = Logger.getLogger(Connection.class);
    static String ABAP_AS_POOLED = "";
    static boolean isTable = false;
    static String structToken;

    public static JCoFunction executeSAPFunction(String functionName, String functionParam) throws JCoException {

        // JCoDestination is the logic address of an ABAP system and ...
        JCoDestination destination = JCoDestinationManager.getDestination(ABAP_AS_POOLED);
        // ... it always has a reference to a metadata repository
        JCoFunction function = destination.getRepository().getFunction(functionName);
        if (function == null)
            throw new RuntimeException(functionName + " Function not found in com.appnomic.appsone.connectors.SAP.");

        // JCoFunction is container for function values. Each function contains separate
        // containers for import, export, changing and table parameters.
        // To set or get the parameters use the APIS setValue() and getXXX().
        // Parameters can be in the form of key1=value1, key2=value2, key3=value3
        // each key value par has to be set separately.
        if (functionParam.equalsIgnoreCase("")) {

        } else {
            StringTokenizer st = new StringTokenizer(functionParam, ",");
            if (st.countTokens() == 0) {
                // This is a scenario where only 1 key value pair is given without delimiter.
                StringTokenizer keyvalue = new StringTokenizer(functionParam, "=");
                if (keyvalue.countTokens() != 2) {
                    throw new RuntimeException(
                            "Wrong Parameter format. should be given in " + "key=value, key=value, ......");
                } else {
                    function.getImportParameterList().setValue(keyvalue.nextToken(), keyvalue.nextToken());
                }
            } else {
                while (st.hasMoreTokens()) {
                    StringTokenizer keyvalue = new StringTokenizer(st.nextToken(), "=");
                    if (keyvalue.countTokens() != 2) {
                        // ASSUMPTION: Case where the input can have a table to be sent
                        // Still to code !!!!!!!

                        throw new RuntimeException(
                                "Wrong Parameter format. should be given in " + "key=value, key=value, ......");
                    } else {
                        function.getImportParameterList().setValue(keyvalue.nextToken(), keyvalue.nextToken());
                    }
                }

            }
        }
        // execute, i.e. send the function to the ABAP system addressed
        // by the specified destination, which then returns the function result.
        // All necessary conversions between Java and ABAP data types
        // are done automatically.
        function.execute(destination);

        return function;
    }

    /*
     *
     *
     */
    public static JCoTable execTableFunction(String functionalMOduleName, String timestamp, String tableName) throws JCoException {
        isTable = false;
        JCoTable tempTble = null;
//        if (inputs.length != 6)
//            throw new RuntimeException("Wrong parameters: Array [0] = functionName Array [1] = functionParam"
//                    + " Array [2] = table name,Array[3] = com.appnomic.appsone.connectors.SAP config file  Array[4] = GMT offset  Array[5] = Mode");
        ABAP_AS_POOLED = ConfigInitializer.DESTINATION_FILE;
        JCoFunction response = executeSAPFunction(functionalMOduleName, timestamp);
//        myDebug("Response Received: " + response.getName());
        log.info("Response Received " + response.getName());
        JCoParameterList tableparams = response.getTableParameterList();

        StringTokenizer myToken = new StringTokenizer(tableName, ":");
//       log.info("mytoken : "+myToken);
        if (myToken.countTokens() == 1)
            isTable = true;
        if (tableparams != null) {
            tempTble = response.getTableParameterList().getTable(myToken.nextToken());
        } else {
//            myDebug("Export List Extracted: ");
            tempTble = response.getExportParameterList().getTable(myToken.nextToken());
        }

        while (myToken.hasMoreTokens()) {
            String subToken = myToken.nextToken();
//            myDebug(subToken + " ::: " + table.getValue(subToken).getClass().getName());
            if (tempTble.getValue(subToken).getClass().getName()
                    .equalsIgnoreCase("com.sap.conn.jco.rt.DefaultStructure")) {
                structToken = subToken;
            } else if (tempTble.getValue(subToken).getClass().getName()
                    .equalsIgnoreCase("com.sap.conn.jco.rt.DefaultTable")) {
                tempTble = tempTble.getTable(subToken);
                if (myToken.countTokens() == 0)
                    isTable = true;
            } else {
                // unknown structure as response or input is wrong
//                    myDebug("ERROR : Wrong Input or output structure / table unknown");
                log.error("ERROR : Wrong Input or output structure / table unknown");
                throw new RuntimeException("Wrong input parameters or output structure / table format not known");
            }
        }
        return tempTble;
    }


}
