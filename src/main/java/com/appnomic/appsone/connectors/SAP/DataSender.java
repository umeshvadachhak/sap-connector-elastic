package com.appnomic.appsone.connectors.SAP;

import com.appnomic.appsone.connectors.utill.ConfigInitializer;
import org.apache.http.HttpHost;
import org.apache.log4j.Logger;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class DataSender {

    private static final Logger log = Logger.getLogger(DataSender.class);
    private static final String hostName = ConfigInitializer.ELASTIC_SEARCH_HOST_NAME;
    private static final Integer port = Integer.parseInt(ConfigInitializer.ELASTIC_SEARCH_PORT);
    private static final String requestTye = ConfigInitializer.ELASTIC_SEARCH_REQUEST_TYPE;
    private static final String transIndex = ConfigInitializer.ELASTIC_SEARCH_ROW_TRANS_INDEX;
    private static final String kpiIndex = ConfigInitializer.ELASTIC_SEARCH_ROW_KPI_INDEX;


    public static String randomString() {
        byte[] array = new byte[7]; // length is bounded by 7
        new Random().nextBytes(array);
        String generatedString = new String(array, Charset.forName("UTF-8"));

//        System.out.println(generatedString);
        return generatedString;
    }

    public static void sendData(List<Map<String, Object>> data, String dataType) throws IOException {
        if (data.isEmpty() || data == null) {
            log.info("There is no data to send");
        } else {
//            for (Map<String, String> documents : data) {
            RestHighLevelClient client = new RestHighLevelClient(
                    RestClient.builder(
                            new HttpHost(hostName, port, requestTye)
                    )
            );
//            IndexRequest request = null;
//
//                if (dataType.equalsIgnoreCase("KPI")){
//                    request = new IndexRequest(kpiIndex);
//                }else {
//                    request = new IndexRequest(transIndex);
//                }
//                request.source(documents);


            BulkRequest bulkRequest = new BulkRequest();
            ;
            for (Map<String, Object> ki : data) {
                log.info("Data to send in elastic-search queue:" + ki);
                if (dataType.equalsIgnoreCase("KPI"))
                    bulkRequest.add(new IndexRequest(kpiIndex).source(ki));
                else
                    bulkRequest.add(new IndexRequest(transIndex).source(ki));
            }
            BulkResponse bulkResponse = client.bulk(bulkRequest, RequestOptions.DEFAULT);
            if (bulkResponse.hasFailures()) {
                log.error(bulkResponse.buildFailureMessage());

            }
            client.close();

//            IndexResponse response = null;
//            try {
//                response = client.index(request, RequestOptions.DEFAULT);
//                String index = response.getIndex();
//                log.info("Successfully sended the data "+index);
//            } catch (IOException e) {
//                log.error("excetion when sending the data to es", e);
//            }
//            try {
//                client.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        }
    }

}
