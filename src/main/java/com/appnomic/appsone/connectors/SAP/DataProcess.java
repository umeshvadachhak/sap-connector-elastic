package com.appnomic.appsone.connectors.SAP;

import com.appnomic.appsone.connectors.utill.ConfigInitializer;
import com.sap.conn.jco.JCoException;
import com.sap.conn.jco.JCoTable;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class DataProcess {
    private static final Logger log = Logger.getLogger(DataProcess.class);

    public static Map<String, List<String>> getJSONFile() {
        Map<String, List<String>> mapForFilter = null;
        JSONParser jsonParser = new JSONParser();
        JSONObject json = null;
        List<String> taskValue = null;
        try {
            FileReader fr = new FileReader(ConfigInitializer.TASK_FILE);
//            log.info("file strea :" + fr.toString());
            json = (JSONObject) jsonParser.parse(fr);
//            System.out.println("JSON DATA :" + json);
            Iterator iterator = json.entrySet().iterator();
            mapForFilter = new HashMap<>();
            while (iterator.hasNext()) {
                taskValue = new ArrayList<>();
                Map.Entry<String, JSONArray> entry = (Map.Entry<String, JSONArray>) iterator.next();
                JSONArray value = entry.getValue();
//                System.out.println("Value :"+value);
                for (int i = 0; i < value.size(); i++) {
//                    System.out.println(value.get(i));
                    taskValue.add((String) value.get(i));
                }
                mapForFilter.put(entry.getKey(), taskValue);
            }
//            System.out.println("Map :"+mapForFilter);
            fr.close();
        } catch (FileNotFoundException e) {
            log.error("Getting file not found exception ", e);
        } catch (IOException e) {
            log.error("Io exception", e);
        } catch (ParseException e) {
            log.error("Parse Exception", e);
        }
        return mapForFilter;
    }


    public static Map<String, String> getColumnListA(String tableName) throws FileNotFoundException {
        Map<String, String> columnList = new LinkedHashMap<>();
        String filename = null;
        filename = System.getProperty("user.dir") + System.getProperty("file.separator") + "/config/" + tableName + ".cfg";
//        colMapFilter = new LinkedHashMap<>();
        Scanner filescanner = new Scanner(new File(filename));
        while (filescanner.hasNext()) {
            String temp = filescanner.nextLine();
//            colFilters.add(temp);
            String[] a = temp.split(" : ");
            if (a.length != 0)
                columnList.put(a[0], a[1]);

        }
//        tableColumnMap.put(tableName, colMapFilter);
        filescanner.close();


        return columnList;
    }

    public static List<Map<String, String>> getKPIData(JCoTable tables, String taskType, Map<String, String> columnList) {
        List<Map<String, String>> kpiData = new ArrayList<>();
        Map<String, String> kpiRow = null;
        for (int k = 0; k < tables.getNumRows(); k++) {
            tables.setRow(k);
            kpiRow = new LinkedHashMap<>();
            if (tables.getString("TASKTYPE").equalsIgnoreCase(taskType)) {
                List<String> columnName = columnList.keySet().stream().collect(Collectors.toList());

                for (int colCount = 0; colCount < columnName.size(); colCount++) {
                    String cname = columnName.get(colCount);

                    if (!tables.isEmpty()) {
                        String temp = tables.getString(cname.trim()).replaceAll("\\s.*", "");
                        kpiRow.put(cname, temp);
                    }
                }
//                System.out.println("KPI row "+kpiRow);
                kpiData.add(kpiRow);
            }
        }

        return kpiData;
    }



    public static List<Map<String, Object>> getTransactionData(JCoTable tables, Map<String, String> columnList, String instanceName, Long time) throws FileNotFoundException, JCoException {
        List<Map<String, Object>> data = new ArrayList<>();
        log.info("getting the transaction data");
        for (int k = 0; k < tables.getNumRows(); k++) {
            tables.setRow(k);
            String rowdata = null;
            List<String> colNames = columnList.keySet().stream().collect(Collectors.toList());
//                System.out.println(colNames);

            Map<String, Object> rowData = new LinkedHashMap<>();
            for (int colCount = 0; colCount < colNames.size(); colCount++) {
                String cname = colNames.get(colCount);
                rowdata = tables.getString(cname).replaceAll("\\s.*", "");
//                        if (eId.isEmpty()||eId==null)
//                            break;
                if (cname.equalsIgnoreCase("RESPTI"))
                    rowData.put(cname, Integer.valueOf(rowdata));
                else
                    rowData.put(cname, rowdata);
            }
            rowData.put("instanceName", instanceName);
            rowData.put("timestamp", time);
            rowData.put("accountId",ConfigInitializer.ACCOUNTID);
            data.add(rowData);
        }

        return data;
    }


    public static List<Map<String, String>> kpiProcess(List<Map<String, String>> rowData, String taskType) {
        List<Map<String, String>> procesedKpi = new ArrayList<>();
        for (int i = 0; i < rowData.size(); i++) {
            Map<String, String> kpiRow = rowData.get(i);
            Map<String, String> processRowKpi = new LinkedHashMap<>();
            for (String rowName : kpiRow.keySet()) {
                switch (taskType) {
                    case "01":
                        processRowKpi.put(rowName, kpiRow.get(rowName));
                        break;
                    case "04":
                        processRowKpi.put("BJ_" + rowName, kpiRow.get(rowName));
                        break;
                    case "65":
                        processRowKpi.put("HTTP_" + rowName, kpiRow.get(rowName));
                        break;
                    case "FE":
                        processRowKpi.put("RFC_" + rowName, kpiRow.get(rowName));
                        break;

                }

            }

            procesedKpi.add(processRowKpi);

        }
        return procesedKpi;
    }


    public static List<Map<String, Object>> convertToDocument(List<Map<String, String>> kpiData, String instanceName, Long gmtTime) {
        List<Map<String, Object>> finalList = new ArrayList<>();
//        DateTimeFormatter formatterForHeal = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:00");
//        LocalDateTime localDateTime = LocalDateTime.parse(gmtTime, formatterForHeal);
        for (int i = 0; i < kpiData.size(); i++) {
            Map<String, String> temMa = kpiData.get(i);
//            System.out.println("Each row of inteal "+temMa);
            Map<String, Object> rowKpiData = null;
            for (String key : temMa.keySet()) {
                rowKpiData = new LinkedHashMap<>();
//                System.out.println(key+"   =   "+temMa.get(key));
//                rowKpiData.put("signature",key+instanceName+gmtTime);
//                rowKpiData.put("kpiName",key);
                rowKpiData.put(key, Integer.valueOf(temMa.get(key)));
                rowKpiData.put("instanceName", instanceName);
                rowKpiData.put("timeStamp", gmtTime);
                finalList.add(rowKpiData);
            }

        }
        return finalList;

    }


}
