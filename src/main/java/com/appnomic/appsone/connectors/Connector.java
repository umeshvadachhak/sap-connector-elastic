package com.appnomic.appsone.connectors;

import com.appnomic.appsone.connectors.SAP.Connection;
import com.appnomic.appsone.connectors.SAP.DataProcess;
import com.appnomic.appsone.connectors.SAP.DataSender;
import com.appnomic.appsone.connectors.utill.ConfigInitializer;
import com.appnomic.appsone.connectors.utill.TimeConverter;
import com.sap.conn.jco.JCoException;
import com.sap.conn.jco.JCoTable;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import static org.quartz.TriggerBuilder.newTrigger;


public class Connector implements Job {
    private static final Logger log = Logger.getLogger(Connector.class);
    private static final String mode = ConfigInitializer.MODE;
    private static final String gmtOffset = ConfigInitializer.TIME_ZONE;
    private static final String configFile = System.getProperty("sapConnector.config.file");
    private static Map<String, String> inputMapping = null;


    public static void main(String[] args) {
        ConfigInitializer.parseConfigFile("config/config.json");
        String log4jConfPath = ConfigInitializer.LOG_FILE;
        PropertyConfigurator.configure(log4jConfPath);
//        mode = ConfigInitializer.MODE;
//        gmtOffset = ConfigInitializer.TIME_ZONE;
        inputMapping = TimeConverter.runTimeDate(ConfigInitializer.TIME_ZONE);
        try {
            log.info("Scheduler is starting");
            //             date job details..
            JobDetail jobDetail = JobBuilder.newJob(Connector.class).withIdentity("StartSAPConnector" + System.currentTimeMillis()).build();
            long millis = System.currentTimeMillis();
            java.util.Date date = new java.util.Date(millis);
            // specifying job trigger that run every 60 sec
//            SimpleTrigger triggers = (SimpleTrigger) newTrigger().withIdentity().

            Trigger trigger = newTrigger()
                    .startAt(date)
                    .withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(ConfigInitializer.FETCH_INTEVAL).repeatForever())
                    .build();
            // Run every 5 seconds.
            log.info("job details: " + jobDetail.getKey().getName());
            // Schedule the job
            SchedulerFactory schedulerFactory = new StdSchedulerFactory();
            Scheduler scheduler = schedulerFactory.getScheduler();
            scheduler.scheduleJob(jobDetail, trigger);
            scheduler.start();

            log.info("trigger info " + trigger.getDescription());


        } catch (SchedulerException e) {
            log.error("This is error in schduler " + e.getStackTrace());
        }

    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        Map<String, Map<String, String>> instanceMapping = ConfigInitializer.INSTANCE_MAPPING;
        JCoTable tempTable = null;
        String gmtTimestamp = null;
        List<Map<String, String>> functionalList = ConfigInitializer.FUNCTIONAL_MODULES;
//        Map<String, String> inputMapping = null;
        DataProcess dataProcess = new DataProcess();
        List<Map<String, Object>> tranData = null;
//        log.info("Running in Run MOde");
//        Map<String, String> localInput = inputMapping;
        inputMapping = TimeConverter.setNewPara(inputMapping);
        log.info("new parameters " + inputMapping);

        ///getting the data from SAP server
        for (String sapInstance : instanceMapping.keySet()) {
            for (int i = 0; i < functionalList.size(); i++) {
                String functionlMOdule = functionalList.get(i).get("module");
                String input = TimeConverter.setInput(inputMapping, functionalList.get(i).get("input"));
                input = input + "," + "SELECT_SERVER=" + sapInstance;
                String tableName = functionalList.get(i).get("table");
                log.info("module to call for getting the data : " + functionlMOdule);
                log.info("input reuired to put in module: " + input);
                log.info("table from where data need to extracted" + tableName);
                try {
                    tempTable = Connection.execTableFunction(functionlMOdule, input, tableName);
                    log.info("Table :" + tempTable);
                } catch (JCoException e) {
                    log.error("Error While Fetching the table ", e);
                }

//                try {
//                    gmtTimestamp = TimeConverter.timestamp(inputMapping, gmtOffset);
//                    log.info("This is GMT timestamp " + gmtTimestamp);
//                } catch (java.text.ParseException e) {
//                    log.error("ERROR While converting the time to GMT", e);
//                }
                log.info("SETTING THE NEW PARAMETER ");
                log.info("parameters " + inputMapping);


                Map<String, String> columnList ;
                List<Map<String, String>> kpiData ;
                List<Map<String, String>> kpiDataWithDataType ;
                List<Map<String, Object>> documentedKpi ;
                String instanceName = instanceMapping.get(sapInstance).get("IntanceName");
                String currntTime = TimeConverter.curruntTime(inputMapping);

                String gmtTime =null;
                try {
                    gmtTime= TimeConverter.timestamp(inputMapping,ConfigInitializer.TIME_ZONE);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Long epochTime = TimeConverter.convertToEpoch(gmtTime);
                log.info("Epoch timestamp is "+epochTime);
                if (tempTable == null || tempTable.isEmpty()){
                    log.info("Table is empty or Not able to retrive the table");
//                    System.exit(1);
                } else {
                for (String dataType : dataProcess.getJSONFile().keySet()) {
                    try {
                        log.info("tasktype is " + dataType);
                        columnList = dataProcess.getColumnListA(tableName);
//                            log.info("Columnlist "+ columnList);
//                            kpiData = DataProcess.getNextDataForNew(tempTable, dataType, columnList);
                        if (columnList.isEmpty() || columnList == null) {
                            log.error("No data to send for KPI processing");
                        } else {
                            kpiData = DataProcess.getKPIData(tempTable, dataType, columnList);
                            if (kpiData.isEmpty()||kpiData==null){
                                log.info("THere is no data for "+dataType+" in the table");
                            }else {
                                log.info("This is kpidata to send for processing: " + kpiData);
                                kpiDataWithDataType = DataProcess.kpiProcess(kpiData, dataType);
                                log.info("total no of rows " + kpiDataWithDataType.size());
                                documentedKpi = DataProcess.convertToDocument(kpiDataWithDataType, instanceName, epochTime);
                                log.info("This is kpi to convert in elastic jsons :" + documentedKpi);
                                DataSender.sendData(documentedKpi, "KPI");
                            }

                        }

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }


//                    log.info("total no of rows "+kpiDataWithFix.size());
//                    if (kpiDataWithFix.isEmpty()||kpiDataWithFix==null){
//                        log.error("There is no documenrts to send on elastic queue");
//                    }
//                    else {
//                        log.info("This is kpi to convert in elastic jsons :" + documentedKpi);
//                        documentedKpi = DataProcess.convertToDocument(kpiDataWithFix, instanceName, epochTime);
//                    }
//
//                    try {
//                        if (documentedKpi.isEmpty()||documentedKpi==null)
//                            log.error("There is no json to send to elastic-search queue");
//                        else
//                            DataSender.sendData(documentedKpi,"KPI");
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }

                    if (tableName.equalsIgnoreCase("USERTCODE")) {

                        try {
                            columnList = dataProcess.getColumnListA("TRANS");
                            assert tempTable != null;
                            tranData = DataProcess.getTransactionData(tempTable, columnList, instanceName, epochTime);
                            DataSender.sendData(tranData, "TRANS");


                        } catch (IOException e) {
                            log.error("FILE NOT FOUNT EXCEPTION", e);
                        } catch (JCoException e) {
                            log.error("JCO exception", e);
                        }


                    }
                }



            }


        }
    }
}
