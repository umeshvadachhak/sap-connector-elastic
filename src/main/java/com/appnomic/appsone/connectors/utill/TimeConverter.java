package com.appnomic.appsone.connectors.utill;

import org.apache.log4j.Logger;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Map;

public class TimeConverter {
    private static final Logger logger = Logger.getLogger(TimeConverter.class);
    private static final DateTimeFormatter formatterForHeal = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:00");

    public static Map<String, String> setNewPara(Map<String, String> para) {

        String date = null;
        Map<String, String> timeMap = new LinkedHashMap<>();
        String startDateTime = null;
        String endDateTime = null;
        startDateTime = para.get("startDate") + para.get("startTime");
        endDateTime = para.get("endDate") + para.get("endTime");
        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HHmmss");
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDateTime endDate = LocalDateTime.parse(endDateTime, inputFormatter).plusMinutes(1);
        LocalDateTime startDate = LocalDateTime.parse(startDateTime, inputFormatter).plusMinutes(1);
        String sDate = startDate.format(dateFormatter);
        String eDate = endDate.format(dateFormatter);
        String sTime = startDate.format(timeFormatter);
        String eTime = endDate.format(timeFormatter);
        timeMap.put("startDate", sDate);
        timeMap.put("startTime", sTime);
        timeMap.put("endDate", eDate);
        timeMap.put("endTime", eTime);
        return timeMap;

    }

//    public static Map<String, String> setToTimeZone(Map<String, String> para, String offset) throws java.text.ParseException {
//        String startTime = null, startDate = null, endDate = null, endTime = null;
//        Map<String, String> timeMap = new LinkedHashMap<>();
//        DateTimeFormatter f = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").withZone(ZoneId.of(offset));
//        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
//        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HHmmss");
//        String enStamp = null;
//        String stStamp = null;
//        stStamp = para.get("startDate") + para.get("startTime");
//        enStamp = para.get("endDate") + para.get("endTime");
//
//        LocalDateTime ldt = LocalDateTime.parse(enStamp, f);
//        LocalDateTime ldts = LocalDateTime.parse(stStamp, f);
//        LocalDateTime startTimeWithZone = ldts.atZone(ZoneId.systemDefault())
//                .withZoneSameInstant(ZoneId.of(offset))
//                .toLocalDateTime();
//        LocalDateTime endTimeWithZone = ldt.atZone(ZoneId.systemDefault())
//                .withZoneSameInstant(ZoneId.of(offset))
//                .toLocalDateTime();
//        endDate = endTimeWithZone.toLocalDate().format(dateFormatter);
//        endTime = endTimeWithZone.toLocalTime().format(timeFormatter);
//        startDate = startTimeWithZone.toLocalDate().format(dateFormatter);
//        startTime = startTimeWithZone.toLocalTime().format(timeFormatter);
//        String finalDate = null;
//
//        timeMap.put("startDate", startDate);
//        timeMap.put("startTime", startTime);
//        timeMap.put("endDate", endDate);
//        timeMap.put("endTime", endTime);
//        logger.info("Load time " + finalDate);
//
////        logger.info("Load time date with Zone : "+ finalDate);
//        return timeMap;
//
//    }



    public static String setInput(Map<String, String> timestamp, String input) {
        String formatedInput = null;
        String[] stringToken = input.split(",");

        if (stringToken.length == 4) {
            String s1 = stringToken[0].replace("yyyymmdd", timestamp.get("startDate"));
            String s2 = stringToken[1].replace("HHmmss", timestamp.get("startTime"));
            String s3 = stringToken[2].replace("yyyymmdd", timestamp.get("endDate"));
            String s4 = stringToken[3].replace("HHmmss", timestamp.get("endTime"));
            formatedInput = s1 + "," + s2 + "," + s3 + "," + s4;
        } else {
            String s1 = stringToken[0].replace("yyyymmddHHmmss", timestamp.get("startDate") + timestamp.get("startTime"));
            String s2 = stringToken[1].replace("yyyymmddHHmmss", timestamp.get("endDate") + timestamp.get("endTime"));
            formatedInput = s1 + "," + s2;
        }
        return formatedInput;
    }


    public static Map<String, String> runTimeDate(String offset) {
//        String timeZone = "GMT+04:30";
        Map<String, String> timeMap = new LinkedHashMap<>();
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyyMMdd");
        DateTimeFormatter timeFormat = DateTimeFormatter.ofPattern("HHmmss");
        LocalDateTime endDateTime = LocalDateTime.now();
        LocalDateTime endDateTimeWithZone = endDateTime.atZone(ZoneId.systemDefault())
                .withZoneSameInstant(ZoneId.of(offset)).toLocalDateTime().withSecond(0);
//        LocalDateTime endDateTime = LocalDateTime.ofInstant(instant,ZoneId.of(offset)).withSecond(0);
        LocalDateTime startDateTimeWithZone = endDateTimeWithZone.minusMinutes(1);
        String endDate = dateFormat.format(endDateTimeWithZone.toLocalDate());
        String endTime = timeFormat.format(endDateTimeWithZone.toLocalTime());
        String startDate = dateFormat.format(startDateTimeWithZone.toLocalDate());
        String startTime = timeFormat.format(startDateTimeWithZone.toLocalTime());
        String finalDate = null;

        timeMap.put("startDate", startDate);
        timeMap.put("startTime", startTime);
        timeMap.put("endDate", endDate);
        timeMap.put("endTime", endTime);
        logger.info("RUN time " + finalDate);
        return timeMap;

    }

    public static Long convertToEpoch(String timestamp) {
        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:00");
        LocalDateTime localDateTime = LocalDateTime.parse(timestamp, inputFormatter);
//        Long timeInSeconds = localDateTime.toEpochSecond(ZoneOffset.UTC);

        Instant instant = localDateTime.atZone(ZoneId.of("GMT")).toInstant();
        Long timeInMillis = instant.toEpochMilli();
        return timeInMillis;
    }

    public static String timestamp(Map<String, String> para, String offset) throws java.text.ParseException {
        String endTimestamp = null;
        String gmtTimestamp = null;
        endTimestamp = para.get("endDate") + " " + para.get("endTime");
        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyyMMdd HHmmss");
        LocalDateTime localDateTime = LocalDateTime.parse(endTimestamp, inputFormatter);
//        endTimeStampToFile = localDateTime.toLocalDate().toString();
//        currentTimestamp = localDateTime.toString();
        ZoneId zone = ZoneId.of(offset);
        ZoneOffset zoneOffSet = zone.getRules().getOffset(localDateTime);
        Instant instant = localDateTime.toInstant(zoneOffSet);
        gmtTimestamp = LocalDateTime.ofInstant(instant, ZoneId.of("GMT")).format(formatterForHeal);
        return gmtTimestamp;

    }

    public static String curruntTime(Map<String, String> para) {
        String endTimestamp = null;
        endTimestamp = para.get("endDate") + " " + para.get("endTime");
        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyyMMdd HHmmss");
        DateTimeFormatter formatterForHeal = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:00");
        LocalDateTime localDateTime = LocalDateTime.parse(endTimestamp,inputFormatter);
//        LocalDateTime localDateTime = LocalDateTime.now();

        endTimestamp = localDateTime.format(formatterForHeal);
//        endTimestamp = localDateTime.toString();
        return endTimestamp;
    }


}